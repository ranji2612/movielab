app.factory('UsersApi', function($http) {
	return {
		login : function(username, password) {
			return $http({
						method: 'POST',
						url: 'https://logs.madstreetden.com/login',
						headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						
						data: {username: username, password: password} });
		},
		logout : function() {
			deleteCookie('currentUser');
			$http({
				withCredentials: true,
				method: 'GET',
				url:'https://logs.madstreetden.com/logout'})
			.success(function(data){})
			.error(function(err){console.log(err);});
			window.location = window.location.origin;
			//return $http.get('https://logs.madstreetden.com/logout');
		},
		changePwd : function(currentPwd, newPwd) {
			return $http({
						method: 'POST',
						url: 'https://logs.madstreetden.com/changePass',
						headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						
						data: {oldPassword: currentPwd, newPassword: newPwd} });
		}
	};
});
var app = angular.module('mainApp', ['ngRoute','ui.router']);
  
//angular-ui routing 
app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/");
  //
  // Now set up the states
  $stateProvider
    .state('welcome', {
		url				:	'/updateMovies',
		templateUrl		:	'html/index.html',
	    controller		:   'indexCtrl',
		data: {
			requireLogin:	false
		}
    })
  	.state('landing', {
		url				:	'/',
		templateUrl		:	'html/landing.html',
	    controller		:   'landingCtrl',
		data: {
			requireLogin:	false
		}
    })
  	.state('search', {
		url				:	'/search',
		templateUrl		:	'html/search.html',
	    controller		:   'searchCtrl',
	  	reloadOnSearch : false,
		data: {
			requireLogin:	false
		}
    })
  	.state('suggest', {
		url				:	'/suggest',
		templateUrl		:	'html/suggest.html',
	    controller		:   'suggestCtrl',
		data: {
			requireLogin:	false
		}
    })
  	.state('movie', {
		url				:	'/movie/:movieId',
		templateUrl		:	'html/movie.html',
	    controller		:   'movieCtrl',
		data: {
			requireLogin:	false
		}
    })
  	.state('usage', {
	  	url				:	'/usage',
		templateUrl		:	'html/usage.html',
		controller		:	'usageCtrl',
	  	data: {
			requireLogin:	false
		}
	});
	
	// use the HTML5 History API
	$locationProvider.html5Mode(true);
});


app.config(['$httpProvider', function ($httpProvider) {
  //Reset headers to avoid OPTIONS request (aka preflight)
	
	$httpProvider.defaults.headers.common = {};
	$httpProvider.defaults.headers.post = {};
	$httpProvider.defaults.headers.put = {};
	$httpProvider.defaults.headers.patch = {};
	
}]);


app.controller('homeCtrl', function ($scope) {
	$scope.message = 'hello world';
	
});

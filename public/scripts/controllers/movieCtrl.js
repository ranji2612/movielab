app.controller('movieCtrl', function ($scope, $http, $stateParams) {
	console.log($stateParams.movieId);
	$http.get('/api/reviews/movie/'+$stateParams.movieId)
	.success(function(data) {
		console.log(data);
		$scope.movie = data[0];
		$scope.ingredients = $scope.movie.ingredients;
	})
	.error(function(err) {
		console.log(err);
	});
	
	
});
app.controller('searchCtrl', function ($scope, $http) {
	console.log('Suggest control');
	$scope.searchStarted = false;
	$scope.showResults = false;
	$scope.noResults = false;
	
	
	$scope.getResults = function() {
		$scope.searchStarted = true;
		$scope.searchKeywordsList = $scope.searchKeywords.toLowerCase().replace(/[:\.,!@#$%^&*()_=]./g,' ').match(/\S+/g).sort();
		$http.get('/api/reviews/search/'+$scope.searchKeywords.toLowerCase() )
		.success( function(data) {
			console.log(data.length);
			if(data.length >0) {
				$scope.searchResults = data;
				$scope.showResults = true;
			} else {
				$scope.noResults = true;
			}
		})
		.error( function(err) {
			console.log(err);
		});
	};

});


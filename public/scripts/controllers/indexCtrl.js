// LoginModalCtrl.js

app.controller('indexCtrl', function ($scope, $http) {

	$scope.message = 'Index page';
	console.log($scope.message);
	
	$scope.init = function() {
		$http.get('/api/reviews')
		.success(function(data) {
			$scope.movieList = data;


		})
		.error(function(err) {
			console.log(err);
		});
	};
	
	$scope.movieId = '';
	$scope.updateDB = function() {
		console.log($scope.movieId);
		
		$http.post('/api/reviews/analyze/'+$scope.movieId, {})
		.success(function(data) {
			console.log(data);
		})
		.error(function(err) {
			console.log(err);
		});
		
	};
	
	$scope.init();
});
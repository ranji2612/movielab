app.filter('numDashRep', function() {
	
	return function(n) {
		if(typeof(n)=="undefined") return "";
		if (n > 999999999) return (n/1000000000).toFixed(1) + 'B';
		if (n > 999999) 	return (n/1000000).toFixed(1) + 'M';
		if (n > 999) 		return (n/1000).toFixed(1) + 'K';
		return n.toFixed(1);
	};
});
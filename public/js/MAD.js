/*
MAD.js 
=======

It has the tracking code which will track all the events made in the carousel or any element.
There is a function called track which takes array as argument.
This function then dumps the data to our logging server (you don't have to worry about this)
So in the customer's website let's say they're having a recommendation panel.
So to crack click or hover on any element, they will call the function like this 

onclick = "track( ['click, 'mhone38343317840'] );"
## called during click of an item

onclick = "track( ['click, 'mhone38343317840', '2', 'mfash41051871820'] );"
## called during click of an item in the carousel. The main product sku, the i'th item and the clicked item's sku

mouseover = "track( ['hover, 'mhone38343317840'] );"
## called during mouseover on an item or any div

onload = "track( ['view, 'mhone38343317840'] );"      
## Called during page load of a particular item


*/

var hov = [];
function track(ei) {
	console.log('------',ei);
    var days = 365; // Number of days to keep cookie alive
    var ru = document.location.href;
    var rf = document.referrer;

    var rest = '';
    if( ru.length > 0 ) {
        if( rf == '' ) {
            rf = '-';
        }

        // If there's a query string, grab it and stick all the parameters on the
        // end.
        var qstring = ru.split( '?' );
        if( qstring.length > 1 ) {
            rest = qstring[ 1 ];
        }

        ru = urlencode( ru );
        rf = urlencode( rf );
        var clicked_time = new Date();
        clicked_time = Math.round(clicked_time.getTime()/1000);

        // Build data.
        var d = 'referrer=' + rf;
        if( ru.length > 0 ) {
            d += '&url=' + ru;
        }
        if( rest.length > 0 ) {
            d += '&' + rest;
        }
        d += '&clicked=' + clicked_time;

        // If the cookie already exists for this bonus code, this isn't a unique hit
        var unique = 1;
        pid = readCookie( 'MADid' );
        if( pid == null || pid == "" ) {
            pid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        }
        setCookie( 'MADid', pid, days, '/' ); // For uniqueness

        d += '&uuid=' + pid;

        

        if (ei.hasOwnProperty('length')){
            for (var e in ei) {
                console.log(ei[e]);
                d += '&events='+ei[e].toString();
            }
        }

        // Now request the 1x1 pixel gif to record the click.
        (new Image()).src =  'https://logs.madstreetden.com/logger?' + d;
    }
    return true;
}

function setCookie( name, value, days, path ) {
    var date = new Date();
    date.setTime( date.getTime() + ( days*24*60*60*1000 ) );
    var expires = "; expires=" + date.toGMTString();
    document.cookie = name + '=' + value + expires + '; path=' + path;
}

function readCookie(cookieName) {
    var theCookie=""+document.cookie;
    var ind=theCookie.indexOf(cookieName);
    if (ind==-1 || cookieName=="") return "";
    var ind1=theCookie.indexOf(';',ind);
    if (ind1==-1) ind1=theCookie.length;
    return unescape(theCookie.substring(ind+cookieName.length+1,ind1));
}

function deleteCookie( cookieName ) {
    if( readCookie( cookieName ) ) {
        setCookie( cookieName, '', 0, '/' );
    }
}

function urlencode(str) {
    str = escape(str);
    str = str.replace(/\+/g, '%2B');
    str = str.replace(/%20/g, '+');
    str = str.replace(/\*/g, '%2A');
    str = str.replace(/\//g, '%2F');
    str = str.replace(/@/g, '%40');
    return str;
}

function hoverCheck(id, ev) {
    if (hov.indexOf(id) == -1) {
        track(ev);
        hov.push(id);
    }
}

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var dbSchema = new Schema({
	"id" : String,
    "title" : String,
    "overallScore" : Number,
    "reviews" : Array,
    "index" : Array,
	"extractedInfo" : Schema.Types.Mixed
  	},{ collection: 'movieReviews' });

module.exports = mongoose.model('reviews', dbSchema);
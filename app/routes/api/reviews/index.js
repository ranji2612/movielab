//Current rate collection
var reviewsColl  = require('./review.model');

var express = require('express');
var router = express.Router();

var mongoose 	= require('mongoose');
var ObjectId 	= mongoose.Types.ObjectId;

// Alchemy --- Temporary for testing
var AlchemyAPI = require('./alchemyapi');
var alchemyapi = new AlchemyAPI();

var currData= {positive:[], negative:[]};
var count=0;
var overallScore = 0;
var currId="";
var index="";
// /api/reviews/

router.get('/movie/:movieId', function( req, res) {
	reviewsColl.find({id:req.params.movieId}, function(err, data) {
		if (err) {
			res.send(err);
		}
		res.json(data);
	});
});

router.get('/search/:searchWords', function(req, res) {
	//Making them regex to match incomplete words
	
	//Aparently the way we can search in $in operator ..
	// Eg. [/a/,/adf/] etc to match keywords with these in them
	var searchWordsList = req.params.searchWords.toLowerCase().replace(/[:\.,!@#$%^&*()_=]/g,' ').match(/\S+/g).sort();
	var regExsearchWordsList=[];
	for (var i in searchWordsList) {
		//Make a regex if only the word is atleast 2 letters
		if (searchWordsList[i].length > 1) {
			regExsearchWordsList.push(RegExp(searchWordsList[i],''));
		} else {
			regExsearchWordsList.push( searchWordsList[i] );
		}
	}
	
	//Return matching results
	reviewsColl.find({ index : { $in : regExsearchWordsList } }, function(err, data) {
		
		if (err) { res.send(err);res.end(); }
		res.send(data);
	});
		
});

router.get('/', function( req, res) {
	reviewsColl.find({}, function(err, data) {
		if (err) {
			res.send(err);
		}
		res.json(data);
	});
});

router.post('/analyze/:movieId', function(req, res) {
	console.log(req.params.movieId);
	
	reviewsColl.find({id:req.params.movieId}, function(err, data) {
		if (err) {
			res.send(err);
		}
		
		//Do this for each film
		//var f = 0;
		//for(f=0;f<data.length;f++) {
			console.log('-------------');
			var reviews = data[0].reviews;
			currData= {positive:[], negative:[]};
			count = 0;
			currId = req.params.movieId;

			for(var i=0;i<reviews.length;i++) {
				
				//Keywords with sentiment API
				alchemyapi.keywords("text", reviews[i], {sentiment:1} , function(response) {
					console.log('********',response);
					var keywords = response.keywords;
					count += 1;
					var reviewScore = 0.0,accKeyCount=0;
					for(var j=0;j<keywords.length;j++) {
						//Add only if relevence > 0.5
						
						if(parseFloat(keywords[j].relevance)>0.5 && Math.abs(parseFloat(keywords[j].sentiment.score))>0.1) {
							
							//Rating based on Sentiment Score
							reviewScore += parseFloat(keywords[j].sentiment.score);
							accKeyCount+=1;
							//Adding to Index - for search and stuff
							index += keywords[j].text+" ";
							
							//Pushinig to DB
							if(keywords[j].sentiment.type==='positive')
							currData.positive.push({text:keywords[j].text,val:parseFloat(keywords[j].sentiment.score),relevence : parseFloat(keywords[j].relevance)});
							else
							currData.negative.push({text:keywords[j].text,val:parseFloat(keywords[j].sentiment.score),relevence : parseFloat(keywords[j].relevance)});
						
						}
					}
					
					//get this reviews score
					console.log('= ',reviewScore/accKeyCount);
					//console.log('---- ',overallScore);
					overallScore += reviewScore/accKeyCount;
					//Update DB when all data is over
					if( count == i) {
						console.log(i,overallScore,overallScore/i);
						
						//Update the DB
						reviewsColl.update({id:currId},{"overallScore": overallScore*1.0/i,"extractedInfo":currData,"index":index.split(/ +/)}, function(err,data) {
							if(err)
								console.log(err);
							console.log('DB Updated');
						});
					}


				});
			}
		//}
		res.sendStatus(200);
	});
	
	
});
module.exports = router;

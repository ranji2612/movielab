module.exports = function(app) {
	
	//api
	app.use('/api/reviews', require('./api/reviews/'));
	
	
	//-- Load the home page for others apart from api
	
	//404 handled at UI routing
	app.get('/*', function(req, res){
		
		res.sendfile('public/html/home.html');	
		
	});
	
};